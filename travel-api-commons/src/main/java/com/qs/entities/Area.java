package com.qs.entities;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Area
 *
 * @author qs
 * @date 2021/3/28 0028
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Area {

    private Integer id;
    private String province;
    private String city;
    private String county;
    @JsonSerialize(using = ToStringSerializer.class)
    private Double longitude;
    @JsonSerialize(using = ToStringSerializer.class)
    private Double latitude;

    /**
     * 计算两个城市之间的距离
     * @param area
     * @return
     */
    public double distanceTo(Area area) {
        Double xDistance = Math.abs(getLongitude() - area.getLongitude());
        Double yDistance = Math.abs(getLatitude() - area.getLatitude());
        return Math.sqrt((xDistance * xDistance) + (yDistance * yDistance));
    }
}
