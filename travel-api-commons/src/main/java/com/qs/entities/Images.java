package com.qs.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 图片实体类
 *
 * @author BEIYE
 * @date 2021/5/5
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Images {

    private Integer mId;
    private Integer postId;
    private String url;

}
