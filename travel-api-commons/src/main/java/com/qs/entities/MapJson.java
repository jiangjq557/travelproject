package com.qs.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

/**
 * MapJson
 *
 * @author qs
 * @date 2021/4/30 0030
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MapJson {

    String value;
    String label;
    List<MapJson> children;
}
