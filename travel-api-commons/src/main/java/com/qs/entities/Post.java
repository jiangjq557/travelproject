package com.qs.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.relational.core.sql.In;

import java.util.Date;
import java.util.List;

/**
 * Post
 *
 * @author qs
 * @date 2021/5/3 0003
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {

    private Integer id;
    private String title;
    private String summary;
    private Integer cityId;
    private String name;
    private String phone;
    private Integer watch;
    private String content;
    private Date createTime;

    private List<Images> images;

}
