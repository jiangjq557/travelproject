package com.qs.entities;

import com.qs.utils.SimulatedAnnealing;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Tour，用来初始化旅行商路径，完成路径的解决方案
 *
 * @author qs
 * @date 2021/5/2 0002
 **/
public class Tour{
    //  保存城市的列表
    private ArrayList tour = new ArrayList<Area>();
    //  保存距离
    private int distance = 0;
    //  生成一个空的路径
    public Tour(){
        for (int i = 0; i < SimulatedAnnealing.allCitys.size(); i++) {
            tour.add(null);
        }
    }

    //  复杂路径
    public Tour(ArrayList tour){
        this.tour = (ArrayList) tour.clone();
    }

    //  获取路径
    public ArrayList getTour(){
        return tour;
    }
    // Creates a random individual
    public void generateIndividual() {

        for (int cityIndex = 0; cityIndex < SimulatedAnnealing.allCitys.size(); cityIndex++) {
            setCity(cityIndex, SimulatedAnnealing.allCitys.get(cityIndex));
        }
        //  随机的打乱
        Collections.shuffle(tour);
    }

    //  获取一个城市
    public Area getCity(int tourPosition) {
        return (Area) tour.get(tourPosition);
    }
    public void setCity(int tourPosition, Area area) {
        tour.set(tourPosition, area);
        //  重新计算距离
        distance = 0;
    }
    //  获得当前的总距离
    public int getDistance(){
        if (distance == 0) {
            int tourDistance = 0;
            for (int cityIndex=0; cityIndex < tourSize(); cityIndex++) {
                Area fromCity = getCity(cityIndex);
                Area destinationCity;
                if(cityIndex+1 < tourSize()){
                    destinationCity = getCity(cityIndex+1);
                }
                else{
                    destinationCity = getCity(0);
                }
                tourDistance += fromCity.distanceTo(destinationCity);
            }
            distance = tourDistance;
        }
        return distance;
    }
    //  获得当前路径中城市的数量
    public int tourSize() {
        return tour.size();
    }

    @Override
    public  String toString() {
        String  printString  =  "|" ;
        for  ( int   i  = 0;  i  < tourSize();  i ++) {
            printString  += getCity( i )+ "|" ;
        }
        return   printString ;
    }
}

