package com.qs.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * User
 *
 * @author qs
 * @date 2020/12/14 0014
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    private Integer id;
    private String phone;
    private String password;
    private String name;
    private String hobby;
    private String sex;
}
