package com.qs.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * jurisdiction
 *
 * @author qs
 * @date 2021/3/28 0028
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class jurisdiction {

    private Integer id;
    private String phone;
    private String jurisdiction;
}
