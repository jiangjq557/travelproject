package com.qs.utils;

import com.qs.entities.Area;
import com.qs.entities.Tour;

import java.util.ArrayList;
import java.util.List;

/**
 * SimulatedAnnealing
 *
 * @author qs
 * @date 2021/5/2 0002
 **/
public class SimulatedAnnealing {
    public static List<Area> allCitys = new ArrayList<Area>();

    // 计算接受的概率（依据热力学原理 P(dE) = exp(dE/(kT)) ）
    public static double acceptanceProbability(int energy, int newEnergy, double temperature) {
        //  如果新的解决方案较优，就接受
        if (newEnergy < energy) {
            return 1.0;
        }
        return Math.exp((energy - newEnergy) / temperature);
    }


    public static void main(String[] args) {
        //  创建所有的城市列表
        List<Area> areaList = new ArrayList<Area>();
        Area  city  =  new  Area(1,"福建省","泉州市","惠安县",118.80, 25.03);
        areaList .add( city );
        Area  city2  =  new  Area(2,"吉林省","吉林市","吉林市",126.55, 43.83);
        areaList .add( city2 );
        Area  city3  =  new  Area(3,"北京市","北京市","天安门",116.40, 39.90);
        areaList .add( city3 );
        List<Area> best = monituihuo(areaList);
//        System.out.println("Final solution distance: " + best.getDistance());
//        for (int i = 0; i < best.tourSize(); i++) {
//            System.out.println("Tour: " + best.getCity(i).getId());
//        }
//        List<Area> bestList = new ArrayList<>();
//        for (int i = 0; i < best.tourSize(); i++) {
//            bestList.add(best.getCity(i));
//        }
//        //最终回到第一个城市
//        bestList.add(bestList.get(0));
        System.out.println("Tour: " + best);
    }


    // 返回近似的最佳旅行路径
    public static List<Area> monituihuo(List<Area> areas) {
        allCitys = areas;
        //  设置初始化温度
        double temp = 10000;
        //  设置冷却概率
        double coolingRate = 0.003;
        //  初始化解决方案
        Tour currentSolution = new Tour();
        currentSolution.generateIndividual();
        System.out.println("Initial solution distance: " + currentSolution.getDistance());
        //  设置当前为最优的方案
        Tour best = new Tour(currentSolution.getTour());
        //  循环直到系统冷却
        while (temp > 1) {
            //  生成一个邻居
            Tour newSolution = new Tour(currentSolution.getTour());
            //  获取随机位置
            int tourPos1 = (int) (newSolution.tourSize() * Math.random());
            int tourPos2 = (int) (newSolution.tourSize() * Math.random());
            Area citySwap1 = newSolution.getCity(tourPos1);
            Area citySwap2 = newSolution.getCity(tourPos2);
            //  交换两个城市
            newSolution.setCity(tourPos2, citySwap1);
            newSolution.setCity(tourPos1, citySwap2);
            //  获得新的解决方案的总路程
            int currentEnergy = currentSolution.getDistance();
            int neighbourEnergy = newSolution.getDistance();
            //  决定是否接受新的方案
            if (acceptanceProbability(currentEnergy, neighbourEnergy, temp) > Math.random()) {
                currentSolution = new Tour(newSolution.getTour());
            }
            //  更新最优方案
            if (currentSolution.getDistance() < best.getDistance()) {
                best = new Tour(currentSolution.getTour());
            }
            //  冷却（即更新当前系统温度）
            temp *= 1 - coolingRate;
        }
        List<Area> bestList = new ArrayList<>();
        for (int i = 0; i < best.tourSize(); i++) {
            bestList.add(best.getCity(i));
        }
        //最终回到第一个城市
        bestList.add(bestList.get(0));
        return bestList;
    }

}