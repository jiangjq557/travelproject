package com.qs.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * FeignConfig
 * OpenFeign提供的日志功能
 *
 * @author qs
 * @date 2021/1/21 0021
 **/

@Configuration
@CrossOrigin
public class FeignConfig {

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
}
