package com.qs.controller;

import com.alibaba.fastjson.JSONArray;
import com.qs.entities.Area;
import com.qs.lang.Result;
import com.qs.service.IPostOrderService;
import com.qs.service.IRouteOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * RouuteController
 *
 * @author qs
 * @date 2021/4/29 0029
 **/

@CrossOrigin
@RestController
@Slf4j
@RequestMapping("/posts")
public class PostOrderController {

    @Resource
    IPostOrderService postOrderService;

    @GetMapping("/selectAll")
    public Result selectAll() {
        return postOrderService.selectAll();
    }

    //接收图片
    @PostMapping("/saveImg")
    @ResponseBody
    public Result images(@RequestParam(value = "file") MultipartFile file) {
        System.out.println(file);
        String resultFile = null;
        try {
            String basePath = "F:\\IdeaProjects\\travel-vue\\public\\img\\";  //与properties文件中lyz.uploading.url相同，未读取到文件数据时为basePath赋默认值
            String ext = "abc" + file.getOriginalFilename();
            String fileName = String.valueOf(System.currentTimeMillis()).concat("_").concat("" + new Random().nextInt(6)).concat(".").concat(ext);
            StringBuilder sb = new StringBuilder();
            //拼接保存路径
            sb.append(basePath).append("/").append(fileName);
            //写到c盘
            File f = new File(sb.toString());
            if (!f.exists()) {
                f.getParentFile().mkdirs();
            }
            OutputStream out = new FileOutputStream(f);
            FileCopyUtils.copy(file.getInputStream(), out);


            //虚拟图片服务器
            String visitUrl = "http://localhost:8080/";
            visitUrl = visitUrl.concat(fileName);
//            f = new File("F:\\IdeaProjects\\travel-vue\\public\\img\\" + fileName);
            resultFile = "/img/" + fileName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.succ(resultFile);
    }
}
