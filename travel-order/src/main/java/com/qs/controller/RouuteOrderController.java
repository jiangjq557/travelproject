package com.qs.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qs.entities.Area;
import com.qs.lang.Result;
import com.qs.service.IRouteOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * RouuteController
 *
 * @author qs
 * @date 2021/4/29 0029
 **/

@CrossOrigin
@RestController
@Slf4j
@RequestMapping("/route")
public class RouuteOrderController {

    @Resource
    IRouteOrderService routeOrderService;

    @GetMapping("/getArea")
    public Result selectAll() {
        return routeOrderService.selectAll();
    }

    @GetMapping("/getPosition/{city}/{county}")
    public Result selectByCounty(@PathVariable(value="city") String city, @PathVariable(value="county") String county) {
        return routeOrderService.selectByCityCounty(city,county);
    }

    @PostMapping("/drawRoute")
    public Result drawRoute(@RequestBody String areaList) {
//        List<Area> areas = (List<Area>) JSON.parse(areaList);
        System.out.println(areaList);
//        JSONObject jsonObject = JSON.parseObject(areaList);
//        System.out.println(jsonObject);
//        JSONArray jsonArray = jsonObject.getJSONArray("areaList");
//        System.out.println(areaList.substring(areaList.indexOf("["),areaList.indexOf("]")));
        //解析格式并去掉反斜杠
        areaList = StringEscapeUtils.unescapeJavaScript(areaList.substring(areaList.indexOf("["),areaList.indexOf("]") + 1));
        System.out.println(areaList);
        List<Area> areas = JSONArray.parseArray(areaList,Area.class);
        System.out.println(areas);
//        return Result.succ(null);
        return routeOrderService.drawRoute(areas);
    }
}
