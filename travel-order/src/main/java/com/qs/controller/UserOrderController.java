package com.qs.controller;

import com.qs.entities.SignUp;
import com.qs.entities.User;
import com.qs.lang.Result;
import com.qs.service.IUserOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * UserOrderController
 *
 * @author qs
 * @date 2021/4/29 0029
 **/
@CrossOrigin
@RestController
@Slf4j
@RequestMapping("/user")
public class UserOrderController {

    @Resource
    private IUserOrderService userOrderService;

    @PostMapping("/sendCode")
    public Result sendMCode(@RequestBody String signUp) {
        log.info("执行sendCode");
        return userOrderService.sendMCode(signUp);
    }

    @PostMapping("/login")
    public Result login(@RequestBody User user) {
        log.info("执行login");
        return userOrderService.login(user);
    }

    @PostMapping("/insert")
    public Result insert(@RequestBody SignUp signUp) {
        return userOrderService.insert(signUp);
    }
}
