package com.qs.service;

import com.qs.entities.Area;
import com.qs.lang.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * IRouteService
 *
 * @author qs
 * @date 2021/4/29 0029
 **/
@CrossOrigin
@Component  //为了可以扫描得到
@FeignClient(value = "travelPost")
public interface IPostOrderService {

    @GetMapping("/posts/selectAll")
    public Result selectAll();
}
