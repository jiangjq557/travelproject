package com.qs.service;

import com.qs.entities.Area;
import com.qs.lang.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * IRouteService
 *
 * @author qs
 * @date 2021/4/29 0029
 **/
@CrossOrigin
@Component  //为了可以扫描得到
@FeignClient(value = "travelRoute")
public interface IRouteOrderService {

    @GetMapping("/route/getArea")
    public Result selectAll();

    @GetMapping("/route/getPosition/{city}/{county}")
    public Result selectByCityCounty(@PathVariable(value="city") String city, @PathVariable(value="county") String county);

    @PostMapping("/route/drawRoute")
    public Result drawRoute(@RequestBody List<Area> areaList);
}
