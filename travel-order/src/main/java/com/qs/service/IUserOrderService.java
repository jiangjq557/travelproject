package com.qs.service;

import com.qs.entities.SignUp;
import com.qs.entities.User;
import com.qs.lang.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * IUserOrderService
 *
 * @author qs
 * @date 2021/4/29 0029
 **/
@CrossOrigin
@Component  //为了可以扫描得到
@FeignClient(value = "travelUser")
public interface IUserOrderService {

    @PostMapping("/user/sendCode")
    public Result sendMCode(@RequestBody String signUp);

    @PostMapping("/user/login")
    public Result login(@RequestBody User user);

    @PostMapping("/user/insert")
    public Result insert(@RequestBody SignUp signUp);
}
