package com.qs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * PostMain8003
 *
 * @author qs
 * @date 2021/5/3 0003
 **/

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class PostMain8003 {

    public static void main(String[] args) {
        SpringApplication.run(PostMain8003.class, args);
    }

}
