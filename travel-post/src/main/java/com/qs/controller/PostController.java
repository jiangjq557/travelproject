package com.qs.controller;

import com.qs.lang.Result;
import com.qs.service.IPostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * PostController
 *
 * @author qs
 * @date 2021/5/3 0003
 **/

@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/posts")
public class PostController {

    @Resource
    IPostService postService;

    @GetMapping("/selectAll")
    public Result selectAll() {
        return Result.succ(postService.selectAll());
    }
}
