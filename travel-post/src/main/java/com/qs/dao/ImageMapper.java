package com.qs.dao;

import com.qs.entities.Images;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 图片查询接口
 *
 * @author BEIYE
 * @date 2021/5/5
 */
@Mapper
@Repository
public interface ImageMapper {

    List<Images> selectByPostId(Integer postid);

}
