package com.qs.dao;

import com.qs.entities.Area;
import com.qs.entities.Post;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PostMapper
 *
 * @author qs
 * @date 2021/5/3 0003
 **/

@Mapper
@Repository
public interface PostMapper {

    Post selectByPrimaryKey(Integer id);

    List<Post> selectAll();
}
