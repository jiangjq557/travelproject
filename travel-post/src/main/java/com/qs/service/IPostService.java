package com.qs.service;

import com.qs.entities.Post;

import java.util.List;

/**
 * IPostService
 *
 * @author qs
 * @date 2021/5/3 0003
 **/
public interface IPostService {

    public List<Post> selectAll();
}
