package com.qs.service.impl;

import com.qs.dao.PostMapper;
import com.qs.entities.Post;
import com.qs.service.IPostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * PostServiceImpl
 *
 * @author qs
 * @date 2021/5/3 0003
 **/

@Service
@Slf4j
public class PostServiceImpl implements IPostService {

    @Resource
    PostMapper postMapper;

    @Override
    public List<Post> selectAll() {
        return postMapper.selectAll();
    }
}
