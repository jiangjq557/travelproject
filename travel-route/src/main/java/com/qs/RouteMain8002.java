package com.qs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * RouteMain8002
 *
 * @author qs
 * @date 2021/4/29 0029
 **/

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class RouteMain8002 {

    public static void main(String[] args) {
        SpringApplication.run(RouteMain8002.class, args);
    }

}
