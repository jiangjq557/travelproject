package com.qs.controller;

import com.qs.entities.Area;
import com.qs.lang.Result;
import com.qs.service.IRouteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * RouteController
 *
 * @author qs
 * @date 2021/4/29 0029
 **/

@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/route")
public class RouteController {

    @Resource
    IRouteService routeService;

    @GetMapping("/getArea")
    public Result selectAll() {
        return Result.succ(routeService.selectAll());
    }

    @GetMapping("/getPosition/{city}/{county}")
    public Result selectByCityCounty(@PathVariable String city, @PathVariable String county) {
        return Result.succ(routeService.selectByCityCounty(city, county));
    }

    @PostMapping("/drawRoute")
    public Result drawRoute(@RequestBody List<Area> areaList) {
        return Result.succ(routeService.drawRoute(areaList));
    }
}
