package com.qs.dao;

import com.qs.entities.Area;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * RouteMapper
 *
 * @author qs
 * @date 2021/4/29 0029
 **/
@Mapper
@Repository
public interface RouteMapper {

    Area selectByPrimaryKey(Integer id);

    Area selectByCounty(String county);

    Area selectByCityCounty(@Param("city")String city, @Param("county") String county);

    List<Area> selectAll();

}
