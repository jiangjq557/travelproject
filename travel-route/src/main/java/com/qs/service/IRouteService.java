package com.qs.service;

import com.qs.entities.Area;
import com.qs.lang.Result;

import java.util.List;

/**
 * IRouteService
 *
 * @author qs
 * @date 2021/4/29 0029
 **/
public interface IRouteService {

    public Result selectAll();

    public Result selectByCityCounty(String city, String county);

    public Result drawRoute(List<Area> areaList);
}
