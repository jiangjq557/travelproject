package com.qs.service.impl;

import com.alibaba.fastjson.JSON;
import com.qs.dao.RouteMapper;
import com.qs.entities.Area;
import com.qs.entities.MapJson;
import com.qs.lang.Result;
import com.qs.service.IRouteService;
import com.qs.utils.SimulatedAnnealing;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * RouteService
 *
 * @author qs
 * @date 2021/4/29 0029
 **/

@Service
@Slf4j
public class RouteServiceImpl implements IRouteService {

    @Resource
    RouteMapper routeMapper;

    @Override
    public Result selectAll() {
        List<Area> areas = routeMapper.selectAll();
        List<MapJson> mapJsonSet = new ArrayList<>();
        List<String> provinceLabels = new ArrayList<>();
        List<String> citysLabels = new ArrayList<>();
        List<String> countyLabels = new ArrayList<>();
        for (Area area : areas) {
            if (!provinceLabels.contains(area.getProvince())) {
                MapJson mapJson = new MapJson();
                mapJson.setValue(String.valueOf(area.getId()));
                mapJson.setLabel(area.getProvince());
                mapJson.setChildren(new ArrayList<>());
                mapJsonSet.add(mapJson);
                provinceLabels.add(area.getProvince());
            }
        }
        for (MapJson mapJson : mapJsonSet) {
            for (Area area : areas) {
                if (mapJson.getLabel().equals(area.getProvince())) {
                    if (!citysLabels.contains(area.getCity())) {
                        MapJson mapJsonCity = new MapJson();
                        mapJsonCity.setValue(String.valueOf(area.getId()));
                        mapJsonCity.setLabel(area.getCity());
                        mapJsonCity.setChildren(new ArrayList<>());
                        mapJson.getChildren().add(mapJsonCity);
                        citysLabels.add(area.getCity());
                    }
                }
            }
        }
        for (MapJson mapJson : mapJsonSet) {
            for (int i = 0; i < mapJson.getChildren().size(); i++) {
                for (Area area : areas) {
                    if (mapJson.getLabel().equals(area.getProvince())) {
                        if (mapJson.getChildren().get(i).getLabel().equals(area.getCity())){
                            if (!countyLabels.contains(area.getCounty())) {
                                MapJson mapJsonCounty = new MapJson();
                                mapJsonCounty.setValue(String.valueOf(area.getId()));
                                mapJsonCounty.setLabel(area.getCounty());
                                mapJson.getChildren().get(i).getChildren().add(mapJsonCounty);
                                countyLabels.add(area.getCounty());
                            }
                        }
                    }
                }
            }
        }
        return Result.succ(JSON.toJSONString(mapJsonSet).replaceAll("\"(\\w+)\"(\\s*:\\s*)", "$1$2"));
    }

    @Override
    public Result selectByCityCounty(String city, String county) {
        return Result.succ(routeMapper.selectByCityCounty(city, county));
    }

    @Override
    public Result drawRoute(List<Area> areaList) {
        return Result.succ(SimulatedAnnealing.monituihuo(areaList));
    }
}
