package com.qs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * com.qs.UserMain8001
 *
 * @author qs
 * @date 2021/3/24 0024
 **/

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class UserMain8001 {

    public static void main(String[] args) {
        SpringApplication.run(UserMain8001.class, args);
    }
}
