package com.qs.controller;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import com.qs.entities.PhoneCheckCode;
import com.qs.entities.SignUp;
import com.qs.entities.User;
import com.qs.lang.Result;
import com.qs.service.IUserService;
import com.qs.utils.AliyunSmsUtils;
import com.qs.utils.NewCode;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import sun.rmi.runtime.Log;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * UserController
 *
 * @author qs
 * @date 2021/4/27 0027
 **/

@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    IUserService userService;

    @Resource
    RabbitTemplate rabbitTemplate;

    @Resource
    RedisTemplate<String, Object> redisTemplate;

    @PostMapping("/sendCode")
    public Result sendMCode(@RequestBody String signUp) {
        JSONObject JO= (JSONObject) JSONObject.parse(signUp);
        String phone = JO.getString("phone");
        log.info(phone);
        NewCode.setNewcode();
        String code = String.valueOf(NewCode.getNewcode());
        PhoneCheckCode phoneCheckCode = new PhoneCheckCode(phone,code);
        if (userService.selectByPhone(phone) == null) {
            rabbitTemplate.convertAndSend("qs.travel", "travel.checkCode", phoneCheckCode);
            rabbitTemplate.convertAndSend("qs.travel", "travel.checkCode", phoneCheckCode);
            rabbitTemplate.convertAndSend("qs.travel", "travel.checkCode", phoneCheckCode);
            log.info("发送成功");
            return Result.succ(200,"发送成功",null);
        } else {
            return Result.fail(400,"手机号已存在",null);
        }
    }

    /**
     * 监听用户注册发送手机验证码
     *
     * @param phoneCheckCode
     * @return
     */
    @RabbitListener(queues = "checkCode")
    public void sendCode(PhoneCheckCode phoneCheckCode) {
        log.info(phoneCheckCode.toString());
        try {
            AliyunSmsUtils.sendSms(phoneCheckCode.getPhone(),phoneCheckCode.getCode());
            AliyunSmsUtils.sendSms(phoneCheckCode.getPhone(),phoneCheckCode.getCode());
            Result result = AliyunSmsUtils.sendSms(phoneCheckCode.getPhone(),phoneCheckCode.getCode());
            log.info(String.valueOf(result));
            redisTemplate.opsForValue().set(phoneCheckCode.getPhone(),phoneCheckCode.getCode(), 5, TimeUnit.MINUTES);
            log.info("发送成功");
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/login")
    public Result login(@RequestBody User user) {
        log.info(String.valueOf(user));
        User userSelect = userService.selectByPhone(user.getPhone());
        if (userSelect != null) {
            if (userSelect.getPassword().equals(user.getPassword())) {
                return Result.succ(200, "登录成功", userSelect);
            } else {
                return Result.fail(403, "用户名或密码错误", null);
            }
        }
        return Result.fail(404,"该手机号码不存在或未注册", null);
    }

    /**
     * 新增用户
     * @param signUp
     * @return
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody SignUp signUp) {
        log.info(String.valueOf(signUp));
        User user = new User(null,signUp.getPhone(),signUp.getPassword(),signUp.getUsername(),null,null);
        return userService.insert(user, signUp.getCode());
    }

}
