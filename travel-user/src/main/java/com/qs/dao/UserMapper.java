package com.qs.dao;

import com.qs.entities.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * UerMapper
 *
 * @author qs
 * @date 2021/3/28 0028
 **/

@Mapper
@Repository
public interface UserMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    User selectByPhone(String phone);


    int updateByPrimaryKeyWithBLOBs(User record);

    int updateByPrimaryKey(User record);

    User findByUsername(String username);

    int updatePassword(User user);

}
