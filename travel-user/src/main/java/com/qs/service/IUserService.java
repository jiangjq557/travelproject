package com.qs.service;

import com.qs.entities.User;
import com.qs.lang.Result;

/**
 * IUserService
 *
 * @author qs
 * @date 2021/4/27 0027
 **/
public interface IUserService {

    User selectByPhone(String phone);

    Result insert(User user, String code);
}
