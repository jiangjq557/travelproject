package com.qs.service.impl;

import com.qs.entities.User;
import com.qs.lang.Result;
import com.qs.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.qs.dao.UserMapper;

import javax.annotation.Resource;

/**
 * UserServiceImpl
 *
 * @author qs
 * @date 2021/4/27 0027
 **/

@Service
@Slf4j
public class UserServiceImpl implements IUserService {

    @Resource
    UserMapper userMapper;

    @Resource
    RedisTemplate<String, Object> redisTemplate;

    /**
     * 新增用户
     *
     * @param user
     * @param code
     * @return
     */
    @Override
    public Result insert(User user, String code) {
        log.info(String.valueOf(user));
        if (code.equals(redisTemplate.opsForValue().get(user.getPhone()))) {
            user.setPhone(String.valueOf(user.getPhone()));
            userMapper.insert(user);
            return Result.succ(200, "注册成功", user);
        }
        return Result.fail(400, "验证码错误", null);
    }

    @Override
    public User selectByPhone(String phone) {
        return userMapper.selectByPhone(phone);
    }


}
